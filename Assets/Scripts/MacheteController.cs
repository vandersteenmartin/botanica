using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacheteController : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Enemy")
    {
      HitboxController hitboxController = col.GetComponent<HitboxController>();
      hitboxController.Die();
    }
  }
}
