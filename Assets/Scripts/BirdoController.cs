using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdoController : MonoBehaviour
{
  private Rigidbody2D rb;

  private bool willFly = false;
  private float flyFactor = 45f;
  private float maxDistanceFromOrigin = 12f;

  private Vector3 origin;

  // Start is called before the first frame update
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    origin = transform.position;
  }

  void Update()
  {
    if (!willFly)
    {
      StartCoroutine(FlyAfterRandomDelay());
      willFly = true;
    }

    if (rb.velocity.x < 0f)
    {
      LookLeft();
    }
    else if (rb.velocity.x > 0f)
    {
      LookRight();
    }
  }

  IEnumerator FlyAfterRandomDelay()
  {
    yield return new WaitForSeconds(Random.Range(2f, 4f));
    Fly();
  }

  void Fly()
  {
    Vector3 flyForce = new Vector3(Random.Range(-1f, 1f), Random.Range(-0.6f, 0.6f), 0f) * flyFactor;

    if (Vector3.Distance(origin, transform.position) < maxDistanceFromOrigin)
    {
      rb.AddForce(flyForce, ForceMode2D.Impulse);
    }
    else
    {
      rb.AddForce((origin - transform.position).normalized * flyFactor, ForceMode2D.Impulse);
    }

    willFly = false;
  }

  void LookLeft()
  {
    Vector3 spriteScale = transform.localScale;
    spriteScale.x = -1 * Mathf.Abs(spriteScale.x);
    transform.localScale = spriteScale;
  }

  void LookRight()
  {
    Vector3 spriteScale = transform.localScale;
    spriteScale.x = Mathf.Abs(spriteScale.x);
    transform.localScale = spriteScale;
  }
}
