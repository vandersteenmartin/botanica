using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
  private float destroyTime = 3.0f;

  private Rigidbody rb;

  void Start()
  {
    Destroy(gameObject, destroyTime);
  }

  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Enemy")
    {
      HitboxController hitboxController = col.GetComponent<HitboxController>();
      hitboxController.Die();
    }
  }
}
