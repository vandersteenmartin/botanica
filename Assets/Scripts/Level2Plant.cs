using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2Plant : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Player")
    {
      PlayerPrefs.SetInt("Level", 3);
      SceneManager.LoadScene("StartScene");
    }
  }
}
