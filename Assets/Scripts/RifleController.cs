using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleController : MonoBehaviour
{
  public GameObject muzzle;
  public Transform player;

  private Rigidbody2D playerRb;

  void Start()
  {
    muzzle = transform.Find("Muzzle").gameObject;
    playerRb = player.GetComponent<Rigidbody2D>();
  }

  void Update()
  {
    Vector3 mousePosition = Input.mousePosition;
    Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

    Vector3 direction = mouseWorldPosition - transform.position;

    if (mouseWorldPosition.x < player.transform.position.x)
    {
      float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle - 180, Vector3.forward);
    }
    else
    {
      float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
      transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
  }

  public void Shoot()
  {
    Vector3 mousePosition = Input.mousePosition;
    Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

    GameObject bullet = (GameObject)Instantiate(Resources.Load("Combat/Bullet"), muzzle.transform.position, muzzle.transform.rotation);
    bullet.GetComponent<Rigidbody2D>().velocity = (mouseWorldPosition - transform.position).normalized * 40f;
  }
}
