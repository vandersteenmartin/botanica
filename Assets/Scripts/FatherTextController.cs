using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FatherTextController : MonoBehaviour
{
  public Text textElement;

  // Start is called before the first frame update
  void Start()
  {
    if (PlayerPrefs.GetInt("Level") == 1)
    {
      textElement.text = "Hello Son ! Thanks for helping me retrieving these rare plants. You'll have to go deep in the jungle to get them but I'm sure you got this !";
    }
    if (PlayerPrefs.GetInt("Level") == 2)
    {
      textElement.text = "Great Job ! Here, take my old boots, they'll make you jump twice as high ! (Double Jump acquired)";
    }
    if (PlayerPrefs.GetInt("Level") == 3)
    {
      textElement.text = "What a nice specimen.. The jungle is getting thicker and thicker, take this machete and clear it all up ! (Left Click)";
    }
    if (PlayerPrefs.GetInt("Level") == 4)
    {
      textElement.text = "I'm impressed, I didn't expect you to retrieve so many specimens, here, take my old gun, it'll prove useful I'm sure (Right click) !";
    }
    if (PlayerPrefs.GetInt("Level") == 5)
    {
      textElement.text = "Golly.. You're cleaning that jungle like there's no tomorrow ! Take this whiskey and take a sip (E Key) when things get tough.. One plant to go!";
    }
    if (PlayerPrefs.GetInt("Level") == 6)
    {
      textElement.text = "Thanks a lot sonny.. This collection is just wonderful... You're the best son ever !";
    }
  }
}
