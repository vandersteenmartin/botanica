using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
  private float destroyTime = 4.0f;

  // Start is called before the first frame update
  void Start()
  {
    Destroy(gameObject, destroyTime);
  }
}
