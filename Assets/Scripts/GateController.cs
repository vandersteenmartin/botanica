using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GateController : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Player")
    {
      if (PlayerPrefs.GetInt("Level") == 1)
      {
        SceneManager.LoadScene("Level1");
      }
      if (PlayerPrefs.GetInt("Level") == 2)
      {
        SceneManager.LoadScene("Level2");
      }
      if (PlayerPrefs.GetInt("Level") == 3)
      {
        SceneManager.LoadScene("Level3");
      }
      if (PlayerPrefs.GetInt("Level") == 4)
      {
        SceneManager.LoadScene("Level4");
      }
      if (PlayerPrefs.GetInt("Level") == 5)
      {
        SceneManager.LoadScene("Level5");
      }
    }
  }
}
