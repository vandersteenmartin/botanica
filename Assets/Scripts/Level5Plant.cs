using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level5Plant : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Player")
    {
      PlayerPrefs.SetInt("Level", 6);
      SceneManager.LoadScene("StartScene");
    }
  }
}
