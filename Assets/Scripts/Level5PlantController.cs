using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level5PlantController : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {
    if (PlayerPrefs.GetInt("Level") > 5)
    {
      SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
      spriteRenderer.enabled = true;
    }
  }
}
