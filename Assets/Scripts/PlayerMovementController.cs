using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
  public Rigidbody2D rb;
  public Transform spriteBody;
  public Animator spriteAnimator;

  public Transform rifle;
  private RifleController rifleController;
  private PlayerHealthController playerHealthController;

  public float fallMultiplier = 2.5f;
  public float lowJumpMultiplier = 2.0f;

  public float gravity = -25f;
  public float airSpeed = 7f;
  public float moveSpeed = 8f;
  public float jumpheight = 3.5f;
  // public float jumpForce = 300f;

  public Transform groundCheck;
  public Transform ceilingCheck;
  public LayerMask groundMask;

  Vector3 velocity;
  private bool isGrounded;
  private bool hittingCeiling;

  // Changed for skills
  public int maxAirJumps = 0;
  public bool dead = false;

  private bool canUseMachete = false;
  private bool macheteReady = true;
  private float macheteCooldown = 0.6f;

  private bool canUseRifle = false;
  private bool rifleReady = true;
  private float rifleCooldown = 1.5f;

  private bool canUseInvincibility = false;
  private bool invincibilityReady = true;

  private int airJumpsLeft = 0;

  // Start is called before the first frame update
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    spriteBody = transform.Find("SpriteBody");
    spriteAnimator = spriteBody.GetComponent<Animator>();
    rifleController = rifle.GetComponent<RifleController>();
    playerHealthController = GetComponent<PlayerHealthController>();
    SpriteRenderer rifleRenderer = rifle.GetComponent<SpriteRenderer>();


    // One game start always go to the village
    // Father conversation based on current level
    // Plant collected shown behind the father based on current level
    // Talking to the father TPs you to the next level

    if (PlayerPrefs.GetInt("Level") == 0)
    {
      PlayerPrefs.SetInt("Level", 1);
    }

    // Give Double Jump
    if (PlayerPrefs.GetInt("Level") > 1)
    {
      maxAirJumps = 1;
    }

    // Give Machete
    if (PlayerPrefs.GetInt("Level") > 2)
    {
      canUseMachete = true;
    }

    // Give Rifle
    if (PlayerPrefs.GetInt("Level") > 3)
    {
      rifleRenderer.enabled = true;
      canUseRifle = true;
      canUseMachete = false;
    }

    // Give Invincibility
    if (PlayerPrefs.GetInt("Level") > 4)
    {
      canUseInvincibility = true;
    }
  }

  // Update is called once per frame
  void Update()
  {
    if (dead) return;

    isGrounded = Physics2D.OverlapBox(groundCheck.transform.position, groundCheck.transform.localScale, 0, groundMask);
    hittingCeiling = Physics2D.OverlapBox(ceilingCheck.transform.position, ceilingCheck.transform.localScale, 0, groundMask);

    float x = Input.GetAxis("Horizontal");

    // PlayerPrefs.SetInt("Level",5);
    // Debug.Log(PlayerPrefs.GetInt("Level"));

    Vector3 mousePosition = Input.mousePosition;
    Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

    if (mouseWorldPosition.x < transform.position.x)
    {
      LookLeft();
    }
    else
    {
      LookRight();
    }

    // Handle movements
    Vector2 move = transform.right * x;

    if (isGrounded)
    {
      velocity.x = (move * moveSpeed).x;

      if (velocity.y < 0)
      {
        velocity.y = 0f;
      }

      // Reset jumpsLeft
      airJumpsLeft = maxAirJumps;
    }
    else
    {
      velocity.x = (move * airSpeed).x;
    }

    // Handle Jumps
    if (Input.GetButtonDown("Jump") && isGrounded)
    {
      velocity.y = Mathf.Sqrt(jumpheight * -2 * gravity);
      // rb.AddForce(new Vector2(0, jumpForce));
    }
    else if (Input.GetButtonDown("Jump") && !isGrounded && (airJumpsLeft > 0))
    {
      velocity.y = Mathf.Sqrt(jumpheight * -2 * gravity);
      // rb.AddForce(new Vector2(0, jumpForce));
      airJumpsLeft--;
    }

    // Handle gravity
    if (!isGrounded)
    {
      if (hittingCeiling && velocity.y > 0.01f)
      {
        velocity.y = -1f;
      }

      if (velocity.y < 0)
      {
        velocity.y += fallMultiplier * gravity * Time.deltaTime;
      }
      else if (velocity.y > 0 && !Input.GetButton("Jump"))
      {
        velocity.y += lowJumpMultiplier * gravity * Time.deltaTime;
      }
      else
      {
        velocity.y += gravity * Time.deltaTime;
      }
    }

    // Handle Animator State
    spriteAnimator.SetBool("isGrounded", isGrounded);
    spriteAnimator.SetFloat("absoluteVelocityX", Mathf.Abs(velocity.x));
    spriteAnimator.SetFloat("velocityY", velocity.y);

    // Machete
    if (canUseMachete && macheteReady && Input.GetButtonDown("Fire1"))
    {
      spriteAnimator.Play("PlayerMacheteSlash");
      macheteReady = false;
      StartCoroutine(ResetMachete());
    }

    // Riffle
    if (canUseRifle && rifleReady && Input.GetButtonDown("Fire2"))
    {
      // spriteAnimator.Play("PlayerMacheteSlash");
      rifleController.Shoot();
      rifleReady = false;
      StartCoroutine(ResetRifle());
    }

    // Invincibility
    if (canUseInvincibility && invincibilityReady && Input.GetButtonDown("Fire3"))
    {
      playerHealthController.Invincible();
      invincibilityReady = false;
      StartCoroutine(ResetInvincibility());
    }

    rb.velocity = velocity;
  }

  void LookLeft()
  {
    Vector3 spriteScale = spriteBody.transform.localScale;
    spriteScale.x = -1 * Mathf.Abs(spriteScale.x);
    spriteBody.transform.localScale = spriteScale;
  }

  void LookRight()
  {
    Vector3 spriteScale = spriteBody.transform.localScale;
    spriteScale.x = Mathf.Abs(spriteScale.x);
    spriteBody.transform.localScale = spriteScale;
  }

  IEnumerator ResetMachete()
  {
    yield return new WaitForSeconds(macheteCooldown);
    macheteReady = true;
  }

  IEnumerator ResetRifle()
  {
    yield return new WaitForSeconds(rifleCooldown);
    rifleReady = true;
  }

  IEnumerator ResetInvincibility()
  {
    yield return new WaitForSeconds(10f);
    invincibilityReady = true;
  }
}
