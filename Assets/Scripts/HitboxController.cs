using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxController : MonoBehaviour
{
  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.tag == "Player")
    {
      PlayerHealthController ph = col.GetComponent<PlayerHealthController>();
      ph.Die();
    }
  }

  public void Die()
  {
    // Play Sound
    Destroy(gameObject, 0f);
  }
}
