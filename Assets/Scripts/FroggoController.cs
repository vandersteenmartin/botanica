using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FroggoController : MonoBehaviour
{
  public Transform groundCheck;
  public Transform ceilingCheck;
  public LayerMask groundMask;

  private Rigidbody2D rb;
  private Animator spriteAnimator;

  private bool isGrounded;
  private bool willJump = false;
  private bool hittingCeiling;

  private float jumpFactor = 50f;
  private Vector3 velocity;

  // Start is called before the first frame update
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    spriteAnimator = GetComponent<Animator>();
  }

  // Update is called once per frame
  void Update()
  {
    velocity = rb.velocity;

    isGrounded = Physics2D.OverlapBox(groundCheck.transform.position, groundCheck.transform.localScale, 0, groundMask);
    hittingCeiling = Physics2D.OverlapBox(ceilingCheck.transform.position, ceilingCheck.transform.localScale, 0, groundMask);

    spriteAnimator.SetBool("isGrounded", isGrounded);

    if (hittingCeiling && velocity.y > 0.01f)
    {
      velocity.y = -1f;
    }

    if (isGrounded && !willJump)
    {
      StartCoroutine(JumpAfterRandomDelay());
      willJump = true;
    }

    if (velocity.x < 0f)
    {
      LookLeft();
    }
    else if (velocity.x > 0f)
    {
      LookRight();
    }


    rb.velocity = velocity;
  }

  IEnumerator JumpAfterRandomDelay()
  {
    yield return new WaitForSeconds(Random.Range(3f, 6f));
    Jump();
  }

  void Jump()
  {
    Vector3 jumpForce = new Vector3(Random.Range(-1f, 1f), 1f, 0f) * jumpFactor;

    rb.AddForce(jumpForce, ForceMode2D.Impulse);

    willJump = false;
  }

  void LookLeft()
  {
    Vector3 spriteScale = transform.localScale;
    spriteScale.x = -1 * Mathf.Abs(spriteScale.x);
    transform.localScale = spriteScale;
  }

  void LookRight()
  {
    Vector3 spriteScale = transform.localScale;
    spriteScale.x = Mathf.Abs(spriteScale.x);
    transform.localScale = spriteScale;
  }
}
