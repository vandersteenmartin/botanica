using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
  public Transform player;

  // Update is called once per frame
  void Update()
  {
    transform.position = new Vector3(Mathf.Lerp(transform.position.x, player.position.x, 0.75f), Mathf.Lerp(transform.position.y, player.position.y, 0.75f), transform.position.z);
  }
}
