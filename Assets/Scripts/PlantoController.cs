using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantoController : MonoBehaviour
{
  private float bulletSpeed = 5f;
  private bool throwingProjectile = false;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if (!throwingProjectile)
    {
      StartCoroutine(SpawnBullet());
      throwingProjectile = true;
    }
  }

  IEnumerator SpawnBullet()
  {
    yield return new WaitForSeconds(0.5f);
    GameObject projectile = (GameObject)Instantiate(Resources.Load("Enemies/Projectile"), transform.position, transform.rotation);
    projectile.GetComponent<Rigidbody2D>().velocity = (new Vector3(Random.Range(-1f, 1f), Random.Range(-0.5f, 0.5f), 0)).normalized * bulletSpeed;

    throwingProjectile = false;
  }
}
