using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnekController : MonoBehaviour
{
  public LayerMask groundMask;

  private Rigidbody2D rb;
  private PolygonCollider2D collider2D;
  private SpriteRenderer spriteRenderer;

  private bool willMove = false;
  private float moveFactor = 2f;

  private float currentVelocityX;

  // Start is called before the first frame update
  void Start()
  {
    rb = GetComponent<Rigidbody2D>();
    spriteRenderer = GetComponent<SpriteRenderer>();
    collider2D = GetComponent<PolygonCollider2D>();
  }

  void Update()
  {
    if (!willMove)
    {
      StartCoroutine(MoveAfterRandomDelay());
      willMove = true;
    }

    // Don't fall
    if (Mathf.Abs(currentVelocityX) > 0)
    {
      Vector3 boxOrigin = collider2D.bounds.center;
      Vector3 direction = Vector3.down;

      boxOrigin.y += collider2D.bounds.size.y / 2;
      if (Mathf.Sign(currentVelocityX) == 1)
      {
        boxOrigin.x += collider2D.bounds.size.x / 2;
        direction += Vector3.right;
      }
      else
      {
        boxOrigin.x -= collider2D.bounds.size.x / 2;
        direction += Vector3.left;
      }

      RaycastHit2D hit = Physics2D.Raycast(boxOrigin, direction, 2f, groundMask);

      if (hit.collider == null)
      {
        currentVelocityX = -currentVelocityX;
      }
    }

    // Turn sprite when appropriate
    if (currentVelocityX < 0f)
    {
      LookLeft();
    }
    else if (currentVelocityX > 0f)
    {
      LookRight();
    }

    Vector3 velocity = rb.velocity;
    velocity.x = currentVelocityX;
    rb.velocity = velocity;
  }

  IEnumerator MoveAfterRandomDelay()
  {
    yield return new WaitForSeconds(Random.Range(1f, 2f));
    Move();
  }

  void Move()
  {
    Vector3 velocity = rb.velocity;

    velocity.x = Random.Range(-1f, 1f) * moveFactor;
    currentVelocityX = velocity.x;

    rb.velocity = velocity;

    willMove = false;
  }

  void LookLeft()
  {
    spriteRenderer.flipX = true;
  }

  void LookRight()
  {
    spriteRenderer.flipX = false;
  }
}
