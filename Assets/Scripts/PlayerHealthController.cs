using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealthController : MonoBehaviour
{
  public Transform player;
  public float dieForce = 10f;

  private PlayerMovementController playerMovementController;
  private Transform spriteBody;

  private bool invincible = false;

  public void Start()
  {
    playerMovementController = player.GetComponent<PlayerMovementController>();
    spriteBody = player.Find("SpriteBody");
  }

  public void Invincible()
  {
    invincible = true;
    foreach (Transform child in spriteBody)
    {
      SpriteRenderer sprite = child.GetComponent<SpriteRenderer>();
      sprite.color = new Color(1f, 1f, 0f, 1f);
    }
    StartCoroutine(ResetInvincibility());
  }

  public void Die()
  {
    // return;
    if (invincible) return;

    player.GetComponent<PlayerMovementController>().dead = true;

    foreach (Transform child in spriteBody)
    {
      SpriteRenderer sprite = child.GetComponent<SpriteRenderer>();
      sprite.color = new Color(0.176f, 0.9137f, 0.13f, 1f);
    }

    Rigidbody2D playerRb = player.GetComponent<Rigidbody2D>();
    playerRb.freezeRotation = false;
    playerRb.gravityScale = 1.0f;

    Vector3 force = Vector3.up * dieForce + Vector3.left * dieForce / 2;

    playerRb.velocity = Vector3.zero;
    playerRb.AddForce(force, ForceMode2D.Impulse);

    playerRb.AddTorque(-0.5f, ForceMode2D.Impulse);

    StartCoroutine(RestartAfterDelay(4.0f));
  }

  IEnumerator RestartAfterDelay(float delay)
  {
    yield return new WaitForSeconds(delay);
    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
  }

  IEnumerator ResetInvincibility()
  {
    yield return new WaitForSeconds(5f);
    invincible = false;

    foreach (Transform child in spriteBody)
    {
      SpriteRenderer sprite = child.GetComponent<SpriteRenderer>();
      sprite.color = new Color(1f, 1f, 1f, 1f);
    }
  }
}
